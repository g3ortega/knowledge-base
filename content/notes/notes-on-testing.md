---
title: "Testing: TL;DRs"
subtitle: ""
date: 2020-08-23
tags: [testing]
---

The later in the development cycle a bug is caught, the more expensive it is; exponentially so in many cases.

Writing automated tests forces you to confront these issues early on in the development cycle. Doing so generally leads to more modular software that enables greater flexibility later on.

Creating and maintaining a robust test suite takes real effort. As a codebase grows, so too will the test suite. It will begin to face challenges like instability and slowness.

A bad test suite can be worse than no test suite at all.

## Benefits of testing code:

* Less debugging
* Increased confidence in changes
* Improved documentation
* Simpler reviews
* Thoughtful design

  Well-designed code should be modular, avoiding tight coupling, and focusing on specific responsibilities. Fixing design issues early often means less rework later.

### On test size

> At Google, we classify every one of our tests into a size of and encourage engineers to always write the smallest possible test for a given piece of functionality... because it keeps the entire test suite running fast and reliably.

### On flaky tests

> Our experience suggests that as you approach 1% flakiness, the tests begin to lose value. At Google, our flaky rate hovers around 0.15%, which implies thousands of flakes every day. We fight hard to keep flakes in check, including actively investing engineering hours to fix them.

> Learning how to isolate and stabilize the effects of randomness is not easy. Sometimes, effects are tied to low-level concerns like hardware interrupts or browser rendering engines. A good automated test infrastructure should help engineers identify and mitigate any non-deterministic behavior.

### On tests properties

> Tests should assume as little as possible about the outside environment, such as the order in which the tests are run.

> A test should contain only the information required to exercise the behavior in question. Keeping tests clear and simple aids the viewers in verifying that the code does what it says it does.

> Code is read far more than it is written, so make sure you write tests you'd like to read!

> Though some other testing strategies make heavy use of test doubles (fakes or mocks) to avoid executing code outside of the system under test, at Google, we prefer to keep the real dependencies in place when it is feasible to do so.
 
> Though some other testing strategies make heavy use of test doubles (fakes or mocks) to avoid executing code outside of the system under test, at Google, we prefer to keep the real dependencies in place when it is feasible to do so.

> Favoring unit tests gives us high confidence quickly, and early in the development process. Larger tests act as sanity checks as the product develops; they should not be viewed as a primary method for catching bugs.

 ## The Beyoncé Rule

 > We are often asked, when coaching new hires, which behaviors or properties actually need to be tested? The straightforward answer is: test everything that you don’t want to break. In other words, if you want to be confident that a system exhibits a particular behavior, the only way to be sure it will is to write an automated test for it. This includes all of the usual suspects like testing performance, behavioral correctness, accessibility, and security. It also includes less obvious properties like testing how a system handles failure.

### Testing for failure

> One of the most important situations a system must account for is failure. Failure is inevitable, but waiting for an actual catastrophe to find out how well a system responds to a catastrophe is a recipe for pain. Instead of waiting for a failure, write automated tests that simulate common kinds of failures. This includes simulating exceptions or errors in unit tests and injecting Remote Procedure Call (RPC) errors or latency in integration and end-to-end tests. It can also include much larger disruptions that affect the real production network using techniques like Chaos Engineering. A predictable and controlled response to adverse conditions is a hallmark of a reliable system.

### On Code Coverage

> An even more insidious problem with code coverage is that, like other metrics, it quickly becomes a goal unto itself. It is common for teams to establish a bar for expected code coverage — for instance, 80%. At first, that sounds eminently reasonable; surely you want to have at least that much coverage. In practice, what happens is that instead of treating 80% like a floor, engineers treat it like a ceiling. Soon, changes begin landing with no more than 80% coverage. After all, why do more work than the metric requires?

> Code coverage can provide some insight into untested code, but it is not a substitute for thinking critically about how well your system is tested.

### Testing at Google Scale

> The openness of our codebase encourages a level of co-ownership that lets everyone take responsibility for the codebase. One benefit of such openness is the ability to directly fix bugs in a product or service you use (subject to approval, of course) instead of complaining about it.

## TL;DRs

* Automated testing is foundational to enabling software to change. About all, if you need to iterate fast without worrying about breaking things.

* For tests to scale, they must be automated.

* A balanced test suite is necessary for maintaining a robust test coverage.

* "If you like it, you should have put a test on it".

* Changing test culture in organizations takes time.

> Winters, Titus. Software Engineering at Google . O'Reilly Media. Kindle Edition. 

