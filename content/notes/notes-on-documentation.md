---
title: "Notes on Documentation"
subtitle: "Some notes about the process of documenting at Google"
date: 2020-08-24
tags: [documentation]
---

Software engineers have the responsibility to create and maintain most of the documentation by themselves. Technical writers and project managers may help, but they won't have the context of an engineer, also it harder to scale. Engineers therefore need the proper tools and incentives to do so effective. 

> The key to making it easier for them to write quality documentation is to introduce processes and tools that scale with the organization and that tie into their existing workflow.

> At Google, out most successful efforts have been when documentation is treated like code and incorporated into the traditional workflow, making it easier for engineers to write and maintain simple documents.

### Why is Documentation Needed?

Quality documentation has tremendous benefits for an engineering organization. Code and APIs become more comprehensible, reducing mistakes. Project teams are more focused when their design goals and team objectives are clearly stated. Manual processes are easier to follow when the steps are clearly outlined. Onboarding new members to a team or code base takes much less effort if the process is clearly documented.

> Unlike testing, which quickly provides benefits to a programmer, documentation generally requires more effort up front and doesn't provide clear benefits to an author until later. But, like investments in testing, the investment made in documentation will pay for itself over time. After all, you might write a document only once, but it will be read hundreds, perhaps thousands of times afterwards; its initial cost is amortized across all future readers.

Not only does documentation scale over time, but it is critical for the rest of the organization to scale as well. It helps answer question like these:

* Why were these design decisions made?
* Why did we implement this code in this manner?
* Why did I implement this code this manner, if you're looking at your own code two years later?

> Instead of forcing engineers to become technical writers, we should instead think about how to make writing documentation easier for engineers.

### Other benefits of documentation

* It helps formulate an API
> If you can't explain it and can't define it, you probably haven't designed it well enough.

* It provides a road map for maintenance and a historical record.

* It makes your code look more professional and drive traffic.

* It will prompt fewer questions from other users. If you have to explain something to someone more than once, it usually makes sense to document that process.

### Documentation is like Code

> Each [programming] language is a tool in a toolbox.

Documentation should be no different: it's a tool, written in a different language (usually English) to accomplish a particular task. Writing documentation is not much different than writing code. Like a programming language, it has rules, a particular syntax, and style decisions, often to accomplish a similar purpose as that within code: enforce consistency, improves clarity, and avoid (comprehension) errors.

### Documentation as code

* Has internal policies or rules to be followed
* Be placed under source control
* Has clear ownership responsible for maintaining the docs
* Undergo reviews for changes (and change with code it documents)
* Have issues tracked, as bugs are tracked in code
* Be periodically evaluated (tested, in some respect)
* If possible, be measured for aspects such as accuracy, freshness, etc.

> Good documentation need not be polished or "perfect". One mistake engineers make when writing documentation is assuming they need to be much better writers.

### On audiences

> As Blaise Pascal once said, "If I had more time, I would have written you a shorter letter.". By keeping a document short and clear, you will ensure that it will satisfy both an expert and a novice.

> A lot of documents at Google begin with a "TL;DR statement" such as "TL;DR: if you are not interested in C++ compilers at Google, you can stop reading now."

### On documentation types

There are several main types of documents that software engineers often need to write:

* Reference documentation, including code comments
* Design documents
* Tutorials
* Conceptual documentation
* Landing pages

### Reference documentation

Reference documentation is the most common type that engineers need to write; indeed, they often need to write some form of reference documents every day. By reference documentation, we mean anything that documents the usage of code within the codebase.

Implementation comments [on the other hand] can assume a lot more domain knowledge of the reader, though be careful in assuming too much: people leave projects, and sometimes it's safer to be methodical about exactly why you wrote this code the way you did.

### Design docs

Most teams at Google requires an approved design document before starting work on any major project.

Some teams require such design documents to be discussed and debated at specific team meetings, where the finer points of the design can be discussed or critiqued by experts. In some respects, these design discussions act as form of code review before any code is written.

The canonical design document templates at Google require engineers to consider aspects of their design such as security implications, internationalization, storage requirements and privacy concerns, and so on. In most cases, such parts of those design documents are reviewed by experts in those domains.

A good design document should cover the goals of the design, it's implementation strategy, and propose key design decisions with an emphasis on their individual trade-offs. The best design documents suggest goals and cover alternatives designs, denoting their strong and weak points.

A good design document, once approved, also acts not only as historical record, but as a measure of whether the project successfully achieved its goals.

### On tutorials

Often, the best time to write a tutorial, if one does not yet exist, is when you first join a team. (It’s also the best time to find bugs in any existing tutorial you are following.) Get a notepad or other way to take notes, and write down everything you need to do along the way, assuming no domain knowledge or special setup constraints; after you’re done, you’ll likely know what mistakes you made during the process — and why — and can then edit down your steps to get a more streamlined tutorial.

Most tutorials require you to perform a number of steps, in order. In those cases, number those steps explicitly. If the focus of the tutorial is on the user (say, for external developer documentation), then number each action that a user needs to undertake.

### Conceptual Documentation

Some code requires deeper explanations or insights than can be obtained simply by reading the reference documentation. In those cases, we need conceptual documentation to provide overviews of the APIs or systems. Some examples of conceptual documentation might be a library overview for a popular API, a document describing the life cycle of data within a server, and so on.

A concept document needs to be useful to a broad audience: both experts and novices alike. Moreover, it needs to emphasize `clarity`, so it often needs to sacrifice completeness (something best reserved for a reference) and (sometimes) strict accuracy.

### Landing pages

> Actually straightforward to fix: ensure that a landing page clearly identifies its purpose, and then include only links to other pages for more information. If something on the landing page is doing more than being a traffic cop, it is not doing its job.

### Documentation Reviews

A technical document benefits from three different types of reviews, each emphasizing different aspects:

* A technical review, for accuracy. This review is usually done by a subject matter expert, often another member of your team. Often, this is part of a code review itself.

* An audience review, for clarity. This is usually someone unfamiliar with the domain. This might be someone new to your team or a customer of your API.

* A writing review, for consistency. This is often a technical writer or a volunteer.

### Case Study related to wikis

> There were problems associated with having most (almost all) engineering documentation contained within a shared wiki: little ownership of important documentation, obsolete information, and difficult in filing bugs or issues with documentation.

### The Beginning, Middle, and End

> All documents - indeed all parts of documents - have a beginning, middle and end. Although it sounds amazingly silly, most documents should have, at a minimum, those three sections.

> Most engineers loathe redundancy, and with good reason. But in documentation, redundancy is often useful.

### The Parameters of Good Documentation

> There are usually three aspects of good documentation: completeness, accuracy, and clarity.

## Conclusion

For any piece of code that you expect to live more than a few months, the extra cycles you put in documenting that code will not only help others, it will help you maintain that code as well.

## TL;DRs

* Documentation is hugely important over time and scale.

* Documentation changes should leverage the existing developer workflow

* Keep documents focused on one purpose

* Write for your audience, not yourself.

